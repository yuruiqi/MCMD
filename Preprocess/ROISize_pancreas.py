import numpy as np
from monai.transforms import *
import json
import matplotlib.pyplot as plt

from ImageProcess.Operations import get_box

with open('/homes/rqyu/Projects/MCMD/Data/Split pancreas/train.json') as f:
    trainlist = json.load(f)
with open('/homes/rqyu/Projects/MCMD/Data/Split pancreas/val.json') as f:
    vallist = json.load(f)
with open('/homes/rqyu/Projects/MCMD/Data/Split pancreas/test.json') as f:
    testlist = json.load(f)

datalist = trainlist + vallist + testlist


def stats_size(datalist):
    hs = []
    ds = []
    ws = []
    for dict in datalist:
        data = LoadNiftid(keys=['image', 'mask'])(dict)

        data = AddChanneld(keys=['image', 'mask'])(data)
        data = Spacingd(keys=['image', 'mask'], pixdim=[0.8, 0.8, 2.5], mode=['bilinear', 'nearest'])(data)

        box = get_box(data['mask'][0], norm=False)
        hs.append(box[3] - box[0])
        ws.append(box[4] - box[1])
        ds.append(box[5] - box[2])

    plt.hist(hs)
    plt.show()
    plt.hist(ws)
    plt.show()
    plt.hist(ds)
    plt.show()


def stats_reso(datalist):
    hs = []
    ds = []
    ws = []
    for dict in datalist:
        data = LoadNiftid(keys=['image'])(dict)

        reso = data['image_meta_dict']['affine']

        hs.append(reso[0,0])
        ws.append(reso[1,1])
        ds.append(reso[2,2])

    plt.hist(hs)
    plt.show()
    plt.hist(ws)
    plt.show()
    plt.hist(ds)
    plt.show()


stats_reso(datalist)
# stats_size(datalist)
