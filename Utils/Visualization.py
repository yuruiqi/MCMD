import torch
import torch.nn as nn
import numpy as np
from Visualization.Image import show_multi_images
from Preprocess.Split import join_path
import os


class FeatureMapSaver:
    """
    model = xxx
    model.layer.register_forward_hook(featuremap_saver.save('f1'))
    model(x)
    featuremap_saver.print('f1')
    """
    def __init__(self, save_dir):
        self.maps_epoch = {}
        self.maps = {}
        self.save_dir = save_dir

        if not os.path.exists(save_dir):
            os.mkdir(save_dir)

    def save(self, name):
        if name not in self.maps_epoch.keys():
            self.maps_epoch[name] = []

        def hook(module, input, output):
            self.maps_epoch[name].append(output.detach().cpu().numpy())
        return hook

    def new_epoch(self):
        for name in self.maps_epoch.keys():
            if name not in self.maps.keys():
                self.maps[name] = []

            self.maps[name].append(np.concatenate(self.maps_epoch[name], axis=0))

    def print(self, name, epoch):
        data = self.maps[name][epoch]  # (batch, n, *)
        for i in range(data.shape[0]):
            imgs = []
            for j in range(data.shape[1]):
                imgs.append({'name':f'{name}_{j}', 'img':data[i][j]})
            show_multi_images(imgs, arrangement=[1, data.shape[1]], save_path=join_path(self.save_dir, f'{name}_{i}.png'))
