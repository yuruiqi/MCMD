import torch


x = torch.ones([20,4])
y = torch.rand([4,4])
y = torch.cat([y.unsqueeze(dim=1),]*5, dim=1)
y = torch.reshape(y, [20, 4])
z = x*y

for i in range(20):
    print(z[i].mean())