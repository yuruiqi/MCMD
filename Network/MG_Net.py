import torch
import torch.nn as nn
from Network.Attention import VarAttention, AttentionGate


def group_cat(x, y, group):
    x_shape = list(x.shape)  # [batch, x*cx, *]
    x = x.reshape([x_shape[0], group, x_shape[1] // group] + x_shape[2:])  # (batch, n, c, h, w, d)
    y = y.reshape([x_shape[0], group, y.shape[1] // group] + x_shape[2:])
    x = torch.cat([x, y], dim=2)  # (batch, n, cx+cy, h, w, d)
    x = x.reshape([x_shape[0], -1] + x_shape[2:])  # (batch, n*(cx+cy), h, w, d)
    return x


class Down(nn.Module):
    def __init__(
        self, 
        in_channel, 
        out_channel, 
        group, 
        mode='3d', 
        two_conv=False,
    ):
        super().__init__()
        self.group = group

        if mode=='3d':
            maxpool = nn.MaxPool3d
            conv = nn.Conv3d
            bn = nn.BatchNorm3d
        else:
            maxpool = nn.MaxPool2d
            conv = nn.Conv2d
            bn = nn.BatchNorm2d

        self.maxpool = maxpool(kernel_size=2, stride=2)
        if two_conv:
            self.conv = nn.Sequential(conv(in_channel, out_channel, kernel_size=3, padding=1, groups=group),
                                      conv(out_channel, out_channel, kernel_size=3, padding=1, groups=group))
        else:
            self.conv = conv(in_channel, out_channel, kernel_size=3, padding=1, groups=group)
        self.bn = bn(out_channel)
        self.prelu = nn.PReLU()

    def forward(self, x):
        x = self.maxpool(x)
        x = self.conv(x)
        x = self.bn(x)
        x = self.prelu(x)
        return x


class Up(nn.Module):
    def __init__(self, in_channel, out_channel, group, mode='3d', two_conv=False, attention=False):
        super().__init__()
        self.group = group
        self.attention = attention

        if mode=='3d':
            self.upsample = nn.Upsample(scale_factor=2, mode='trilinear', align_corners=True)
            conv = nn.Conv3d
            bn = nn.BatchNorm3d
        else:
            self.upsample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            conv = nn.Conv2d
            bn = nn.BatchNorm2d

        if two_conv:
            self.conv = nn.Sequential(conv(in_channel, out_channel, kernel_size=3, padding=1, groups=group),
                                      conv(out_channel, out_channel, kernel_size=3, padding=1, groups=group))
        else:
            self.conv = conv(in_channel, out_channel, kernel_size=3, padding=1, groups=group)
        self.bn = bn(out_channel)
        self.prelu = nn.PReLU()

        if attention:
            # TODO: confirm arg:out_channel
            self.attention_gate = AttentionGate(int(in_channel/3), int(in_channel*2/3), in_channel, group=group)

    def forward(self, x, y):
        if self.attention:
            alpha = self.attention_gate(y, x)  # (batch, n, *)
            alpha = torch.cat([alpha.unsqueeze(dim=2), ] * (y.shape[1] // self.group), dim=2)  # (batch, n, c, *)
            alpha = torch.reshape(alpha, y.shape)  # (batch, n*c, *)
            y = alpha*y

        x = self.upsample(x)
        x = group_cat(x, y, self.group)
        x = self.conv(x)
        x = self.bn(x)
        x = self.prelu(x)
        return x


class MGNet(nn.Module):
    def __init__(
        self, 
        in_channel, 
        filters, 
        group, 
        mode='3d', 
        two_conv=False,
        share_conv=False,
        attention=False,
        dropout=False
    ):
        super().__init__()
        if mode=='3d':
            conv = nn.Conv3d
            bn = nn.BatchNorm3d
        else:
            conv = nn.Conv2d
            bn = nn.BatchNorm2d

        self.group = group
        self.dropout = dropout
        if two_conv:
            self.inc = nn.Sequential(conv(in_channel, filters[0], kernel_size=3, padding=1, groups=group),
                                     bn(filters[0]),
                                     nn.PReLU(),
                                     conv(filters[0], filters[0], kernel_size=3, padding=1, groups=group),
                                     bn(filters[0]),
                                     nn.PReLU())
        else:
            self.inc = nn.Sequential(conv(in_channel, filters[0], kernel_size=3, padding=1, groups=group),
                                     bn(filters[0]),
                                     nn.PReLU())

        self.down1 = Down(filters[0], filters[1], group=group, mode=mode, two_conv=two_conv)
        self.down2 = Down(filters[1], filters[2], group=group, mode=mode, two_conv=two_conv)
        self.down3 = Down(filters[2], filters[3], group=group, mode=mode, two_conv=two_conv)
        self.down4 = Down(filters[3], filters[4], group=1 if share_conv else group, mode=mode, two_conv=two_conv)
        self.up1 = Up(filters[4]+filters[3], filters[3], group=group, mode=mode, two_conv=two_conv, attention=attention)
        self.up2 = Up(filters[3]+filters[2], filters[2], group=group, mode=mode, two_conv=two_conv, attention=attention)
        self.up3 = Up(filters[2]+filters[1], filters[1], group=group, mode=mode, two_conv=two_conv, attention=attention)
        self.up4 = Up(filters[1]+filters[0], filters[0], group=group, mode=mode, two_conv=two_conv, attention=attention)

        if self.dropout:
            self.drop1 = nn.Dropout(p=0.1)
            self.drop2 = nn.Dropout(p=0.2)
            self.drop3 = nn.Dropout(p=0.3)
            self.drop4 = nn.Dropout(p=0.4)

        if two_conv:
            self.class_conv = nn.Sequential(conv(filters[0], filters[0], kernel_size=3, padding=1, groups=group),
                                            bn(filters[0]),
                                            nn.PReLU(),
                                            conv(filters[0], group, kernel_size=3, padding=1, groups=group))
        else:
            self.class_conv = conv(filters[0], group, kernel_size=3, padding=1, groups=group)

    def forward(self, x):
        """
        x: (batch, 1, h, w, d) or (batch, n*1, h, w, d) if group_in.
        """
        # TODO: check
        if x.shape[1] < self.group:
            dim = len(x.shape[2:])
            x = x.repeat([1,self.group] + [1,]*dim)

        x1 = self.inc(x)  # [batch, n*c1, h, w, d]
        if self.dropout:
            x1 = self.drop1(x1)
        x2 = self.down1(x1)  # [batch, n*c2, h, w, d]
        if self.dropout:
            x2 = self.drop2(x2)
        x3 = self.down2(x2)  # [batch, n*c3, h, w, d]
        if self.dropout:
            x3 = self.drop3(x3)
        x4 = self.down3(x3)  # [batch, n*c4, h, w, d]
        if self.dropout:
            x4 = self.drop4(x4)
        x5 = self.down4(x4)  # [batch, c5, h, w, d]
        
        x = self.up1(x5, x4)  # [batch, n*c4, h, w, d]
        x = self.up2(x, x3)  # [batch, n*c3, h, w, d]
        x = self.up3(x, x2)  # [batch, n*c2, h, w, d]
        x = self.up4(x, x1)  # [batch, n*c1, h, w, d]
        x = self.class_conv(x)  # [batch, n, h, w, d] before sigmoid
        x = torch.sigmoid(x)

        return x
