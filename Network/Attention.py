import torch
import torch.nn as nn


def compute_var_map(x):
    """
    x: (batch, n, c, *)
    """
    x = x.var(dim=1)  # (batch, c, *)
    x = x.mean(dim=1, keepdim=True)  # (batch, 1, *)
    return x


class VarAttention(nn.Module):
    def __init__(self, mode):
        super().__init__()
        self.mode = mode

        if mode == 'all':
            self.conv1 = nn.Conv2d(3, 1, 3, padding=1, bias=False)
        elif mode == 'var':
            self.conv1 = nn.Conv2d(1, 1, 3, padding=1, bias=False)
        self.sigmoid = nn.Sigmoid()

        nn.init.kaiming_normal_(self.conv1.weight, a=0, mode='fan_in')

    def forward(self, x, group):
        """
        x: (batch, n*c, *)
        """
        avg_out = torch.mean(x, dim=1, keepdim=True)  # (batch, 1, *)
        max_out, _ = torch.max(x, dim=1, keepdim=True)

        shape = list(x.shape)
        x = x.reshape([shape[0], group, shape[1] // group] + shape[2:])  # (batch, n, c, *)

        var_out = compute_var_map(x)  # (batch, 1, *)

        if self.mode == 'all':
            x = torch.cat([avg_out, max_out, var_out], dim=1)
        else:
            x = var_out

        x = self.conv1(x)

        return self.sigmoid(x)


class AttentionGate(nn.Module):
    def __init__(self, in_channel_x, in_channel_g, out_channel, group):
        super().__init__()
        self.group = group
        # TODO: 3d
        self.conv_g = nn.Conv2d(in_channel_g, out_channel, kernel_size=1, groups=group)
        self.conv_x = nn.Sequential(nn.Conv2d(in_channel_x, out_channel, kernel_size=1, groups=group),
                                    nn.MaxPool2d(kernel_size=2, stride=2))
        self.conv = nn.Sequential(nn.Conv2d(out_channel, group, kernel_size=1, groups=group),
                                  nn.Sigmoid(),
                                  nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True))

    def forward(self, x, g):
        """
        x: (batch, n*c, *) bigger size
        g: smaller size
        """
        merge = self.conv_x(x)+self.conv_g(g)  # (batch, out_channel, *)
        alpha = self.conv(torch.relu(merge))  # (batch, n, *)
        return alpha
